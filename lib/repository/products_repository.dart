import 'dart:async';
import '../models/product_model.dart';
import '../api/api_service.dart';

class ProductsRepository {
  final ApiService apiService;

  ProductsRepository({required this.apiService});

  Future<List<ProductModel>> getProducts({String? searchQuery}) async {
    return await apiService.getProducts(searchQuery: searchQuery);
  }
}
