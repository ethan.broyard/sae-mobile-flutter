import 'dart:convert';
import 'package:http/http.dart' as http;
import '../models/product_model.dart';

class ApiService {
  static const String _baseUrl = 'https://fakestoreapi.com';

  Future<dynamic> get(String endpoint) async {
    final response = await http.get(Uri.parse('$_baseUrl/$endpoint'));

    if (response.statusCode == 200) {
      final parsed = jsonDecode(response.body);
      return parsed;
    } else {
      throw Exception('Failed to load data from API');
    }
  }

  Future<List<ProductModel>> getProducts({String? searchQuery}) async {
    final response = await get('products');

    if (response is List) {
      List<ProductModel> products = response
          .map<ProductModel>((json) => ProductModel.fromJson(json))
          .toList();

      if (searchQuery != null && searchQuery.isNotEmpty) {
        products = products
            .where((product) =>
                product.title.toLowerCase().contains(searchQuery.toLowerCase()))
            .toList();
      }

      return products;
    } else {
      throw Exception('Failed to load products');
    }
  }
}
