import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './ui/onboarding_view.dart';
import './ui/products_view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sae_mobile_flutter/ui/onboarding_view.dart';

void main() {
  _resetOnboardingPreference();
  runApp(MyApp());
}

Future<bool> _getOnboardingPreference() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getBool('skip_onboarding') ?? false;
}
void _resetOnboardingPreference() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.remove('skip_onboarding');
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: FutureBuilder<bool>(
        
  future: _getOnboardingPreference(),
  builder: (context, snapshot) {
    if (snapshot.connectionState == ConnectionState.done) {
      if (snapshot.data == true) {
        return ProductsView();
      } else {
        return OnboardingViewStatefulWidget(onFinish: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => ProductsView()),
          );
        });
      }
    } else {
      return const Scaffold(
        body: Center(child: CircularProgressIndicator()),
      );
    }
  },
),
    );
  }
}
