import 'package:flutter/material.dart';
import '../models/product_model.dart';
import '../repository/products_repository.dart';
import '../api/api_service.dart';
import '../ui/detail_view.dart';
import '../ui/cart_view.dart';
import 'package:sae_mobile_flutter/ui/product_search_delegate.dart';

class ProductsView extends StatefulWidget {
  const ProductsView({Key? key}) : super(key: key);

  @override
  _ProductsViewState createState() => _ProductsViewState();
}

class _ProductsViewState extends State<ProductsView> {
  final ProductsRepository _productsRepository =
      ProductsRepository(apiService: ApiService());
  late Future<List<ProductModel>> _futureProducts;

  Map<int, int> _cartItems = {};

  @override
  void initState() {
    super.initState();
    _futureProducts = _productsRepository.getProducts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Products'),
        actions: [
          IconButton(
      icon: Icon(Icons.search),
      onPressed: () async {
        final ProductModel? selectedProduct = await showSearch<ProductModel?>(
          context: context,
delegate: ProductSearchDelegate(productsRepository: _productsRepository),
);
            if (selectedProduct != null) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => DetailView(product: selectedProduct),
            ),
                );
              }
            },
          ),
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => CartView(cartItems: _cartItems)),
              );
            },
          ),
        ],
      ),
      body: FutureBuilder<List<ProductModel>>(
        future: _futureProducts,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final List<ProductModel> products = snapshot.data!;

            return ListView.builder(
              itemCount: products.length,
              itemBuilder: (context, index) {
                final ProductModel product = products[index];
                return ListTile(
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage(product.image),
                  ),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(product.title),
                      IconButton(
                        icon: Icon(Icons.add),
                        onPressed: () {
                          setState(() {
                            if (_cartItems.containsKey(product.id)) {
                              _cartItems.update(
                                  product.id, (value) => value + 1,
                                  ifAbsent: () => 1);
                            } else {
                              _cartItems[product.id] = 1;
                            }
                          });
                        },
                      ),
                    ],
                  ),
                  subtitle: Text(product.description),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DetailView(product: product)),
                    );
                  },
                );
              },
            );
          } else if (snapshot.hasError) {
            return Center(child: Text('${snapshot.error}'));
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
