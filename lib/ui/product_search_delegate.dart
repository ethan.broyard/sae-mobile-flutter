// product_search_delegate.dart

import 'package:flutter/material.dart';
import '../models/product_model.dart';
import '../repository/products_repository.dart';
import '../api/api_service.dart';

class ProductSearchDelegate extends SearchDelegate<ProductModel?> {
  final ProductsRepository _productsRepository;

    ProductSearchDelegate({required ProductsRepository productsRepository})
      : _productsRepository = productsRepository;

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

      @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }



   @override
  Widget buildSuggestions(BuildContext context) {
    return Container(); 
  }

  @override
  Widget buildResults(BuildContext context) {
    if (query.isEmpty) {
      return Center(child: Text('Veuillez entrer un terme de recherche'));
    }

    return FutureBuilder<List<ProductModel>>(
      future: _productsRepository.getProducts(searchQuery: query),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else if (!snapshot.hasData) {
          return Center(child: Text('Aucun produit trouvé'));
        } else {
          final products = snapshot.data!;
          return ListView.builder(
            itemCount: products.length,
            itemBuilder: (context, index) {
              final product = products[index];
              return ListTile(
                title: Text(product.title),
                onTap: () {
                  close(context, product);
                },
              );
            },
          );
        }
      },
    );
  }

}
