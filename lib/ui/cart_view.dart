import 'package:flutter/material.dart';

class CartView extends StatefulWidget {
  final Map<int, int> cartItems;

  const CartView({Key? key, required this.cartItems}) : super(key: key);

  @override
  _CartViewState createState() => _CartViewState();
}

class _CartViewState extends State<CartView> {
  late Map<int, int> _cartItems;

  @override
  void initState() {
    super.initState();
    _cartItems = Map.from(widget.cartItems);
  }

  void _decrementItem(int productId) {
    setState(() {
      if (_cartItems[productId]! > 0) {
        _cartItems.update(productId, (value) => value - 1, ifAbsent: () => 0);

      }
    });
  }

  void _incrementItem(int productId) {
    setState(() {
      _cartItems.update(productId, (value) => value + 1, ifAbsent: () => 1);

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Cart'),
      ),
      body: ListView.builder(
        itemCount: _cartItems.length,
        itemBuilder: (context, index) {
          final int productId = _cartItems.keys.toList()[index];
          final int quantity = _cartItems[productId]!;
          return ListTile(
            title: Text('Product $productId'),
            subtitle: Text('Quantity: $quantity'),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                IconButton(
                  icon: const Icon(Icons.remove),
                  onPressed: () => _decrementItem(productId),
                ),
                IconButton(
                  icon: const Icon(Icons.add),
                  onPressed: () => _incrementItem(productId),
                ),
              ],
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _cartItems.clear();
          });
        },
        child: const Icon(Icons.check),
      ),
    );
  }
}
