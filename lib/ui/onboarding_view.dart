import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OnboardingViewStatefulWidget extends StatefulWidget {
  final Function onFinish;

  const OnboardingViewStatefulWidget({required this.onFinish, Key? key}) : super(key: key);

  @override
  _OnboardingViewState createState() => _OnboardingViewState();
}

class _OnboardingViewState extends State<OnboardingViewStatefulWidget> {
  bool skipOnboarding = false;

  void _updateOnboardingPreference(bool skip) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('skip_onboarding', skip);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Écran d\'accueil'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'Bienvenue dans notre application! ',
              style: TextStyle(fontSize: 40),
            ),
            const Text(
              '\nVoici quelques informations sur l\'utilisation de l\'application:',
              style: TextStyle(fontSize: 22),
            ),
            const Text(
              '\nVous pouvez consultez les details des articles en cliquant dessus,\n\n Vous pouvez les ajouter au panier en cliquant sur le +,\n\n Vous avez la possibilité de rechercher un article grâce à la loupe en haut à droite, Entrez le nom et appuyer sur entrée,\n\n Enfin, Vous pouvez consultez votre panier en cliquant en haut à droite sur l\'icône aproprié.\n\n Bonne continuation\n',
              style: TextStyle(fontSize: 18),
            ),
            CheckboxListTile(
              title: const Text('Ne plus afficher cet écran'),
              value: skipOnboarding,
              onChanged: (bool? value) {
                setState(() {
                  skipOnboarding = value!;
                });
              },
            ),
            ElevatedButton(
              onPressed: () {
                _updateOnboardingPreference(skipOnboarding);
                widget.onFinish();
              },
              child: const Text('Terminer'),
            ),
          ],
        ),
      ),
    );
  }
}
