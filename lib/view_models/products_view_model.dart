import 'package:flutter/material.dart';
import '../models/product_model.dart';
import '../repository/products_repository.dart';

class ProductsViewModel extends ChangeNotifier {
  final ProductsRepository _productsRepository;

  ProductsViewModel({required ProductsRepository productsRepository})
      : _productsRepository = productsRepository;

  late List<ProductModel> _products = [];
  late List<ProductModel> _favoriteProducts = [];

  List<ProductModel> get products => _products;
  List<ProductModel> get favoriteProducts => _favoriteProducts;

  Future<void> fetchProducts() async {
    try {
      _products = await _productsRepository.getProducts();
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  bool isFavorite(ProductModel product) {
    return _favoriteProducts.contains(product);
  }

  void toggleFavorite(ProductModel product) {
    if (_favoriteProducts.contains(product)) {
      _favoriteProducts.remove(product);
    } else {
      _favoriteProducts.add(product);
    }
    notifyListeners();
  }

  Future<void> loadFavorites() async {
    try {
      _favoriteProducts = await _productsRepository.getFavorites();
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }
}
