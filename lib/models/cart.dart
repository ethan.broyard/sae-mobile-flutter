import '../models/product_model.dart';
import '../models/cartItem.dart';
import '../api/api_service.dart';

class Cart {
  final List<CartItem> _items = [];

  List<CartItem> get items => _items;

  void addItem(ProductModel product) {
    final existingItem = _items.firstWhere(
        (item) => item.product.id == product.id,
        orElse: () => null);

    if (existingItem != null) {
      existingItem.incrementQuantity();
    } else {
      _items.add(CartItem(product: product));
    }
  }

  void removeItem(CartItem item) {
    _items.remove(item);
  }

  double get total => _items.fold(
      0, (previousValue, item) => previousValue + item.total);
}
