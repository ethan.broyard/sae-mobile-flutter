import '../models/product_model.dart';
import '../models/cart.dart';
import '../api/api_service.dart';

class CartItem {
  final ProductModel product;
  int quantity;
  double total;

  CartItem({required this.product, this.quantity = 1})
      : total = product.price * quantity;

  void incrementQuantity() {
    quantity++;
    total = product.price * quantity;
  }

  void decrementQuantity() {
    if (quantity > 1) {
      quantity--;
      total = product.price * quantity;
    }
  }
}
